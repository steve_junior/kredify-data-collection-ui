require('dotenv').config()

module.exports = {
    env: {
        'API_URL'   : process.env.API_URL,
        'TAWK_TO_PROPERTY_ID' : process.env.TAWK_TO_PROPERTY_ID,
        'DEBUG'  : process.env.DEBUG,
        'ENV'    : process.env.ENV,
        'GA_TRACKING_ID' : process.env.GA_TRACKING_ID,
        'SITE_KEYWORDS'  : process.env.SITE_KEYWORDS,
        'SITE_DESCRIPTION' : process.env.SITE_DESCRIPTION,
        'APP_URL'          : process.env.APP_URL,
        'PRODUCTION_APP_URL' : process.env.PRODUCTION_APP_URL,
    }
};