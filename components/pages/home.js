import React from 'react';
import {Company, routeTo} from "../../src/helpers";
import {Container, DescriptionImage, Section, StatementBlock} from "../Section";
import {Column, Row} from "../Footer";
import {GetStartedClientRoute} from "../../src/custom-routes";
import {RegularNavigation} from "../Navbars";

const Home = (props) => {

    return <React.Fragment>

        <RegularNavigation />

        <Section classnames={`bg-primary-3 text-white pb-0 o-hidden`}>
            <Container>
                <Row classnames={`row justify-content-between align-items-center`}>
                    <Column classnames={`col-xl-5 col-lg-6 text-center text-lg-left mb-4 mb-md-5 mb-lg-0`}>
                        <h1 className="display-3">buy now <br/> pay later <br/> any where</h1>
                        <p className="lead">
                            Afford high-priced products without breaking your bank. <br/>
                            {/*Pay later in cool installments.*/}
                             Pay in installments for all your shopping.
                        </p>
                        <a type="button" className="btn btn-lg btn-danger" onClick={e => { e.preventDefault(); routeTo(GetStartedClientRoute) }}>
                            Sign Up
                        </a>
                    </Column>
                    <Column>
                        <img src={`assets/img/blog/thumb-12.jpg`} alt="Screenshot"
                             className="img-fluid rounded shadow-lg border"/>
                    </Column>
                </Row>
            </Container>
            <div className="divider divider-bottom bg-warning-alt"/>
        </Section>

        <Section classnames={`bg-primary-3 text-white`}>
            <Container>
                <Row classnames={`row section-title justify-content-center text-center`}>
                    <Column classnames={`col-md-9 col-lg-8 col-xl-7`}>
                        <h3 className="display-4">why join us</h3>
                    </Column>
                </Row>
                <Row classnames={`row text-center justify-content-center`}>
                    <Column classnames={`col-md-6 col-lg-6 mb-4 mb-md-5`} data-aos="fade-up" data-aos-delay="100">
                        <div className="mx-xl-4">
                            <img src="assets/img/logos/product/kyan.svg" alt="Kyan logo" className="mb-4" />
                            {/*<div className={`lead`}>*/}
                                {/*Mission*/}
                            {/*</div>*/}
                            <div className={`text-center text-white lead`}>With {Company().name}, anyone can first receive their purchase
                                and pay off smartly, gradually and conveniently
                            </div>
                        </div>
                    </Column>

                    <Column classnames={`col-md-6 col-lg-6 mb-4 mb-md-5`} data-aos="fade-up" data-aos-delay="100">
                        <div className="mx-xl-4">
                            <img src="assets/img/logos/product/kyan.svg" alt="Kyan logo" className="mb-4" />
                            {/*<div className={`lead`}>*/}
                                {/*Partner*/}
                            {/*</div>*/}
                            <div className={`text-center text-white lead`}>
                                Your customers will love this new way of payment. <br/> Partner with {Company().name} to access the largest
                                poll of excited shoppers and increase your revenue off the roof
                            </div>
                        </div>
                    </Column>
                </Row>
            </Container>
        </Section>

        <Section classnames="o-hidden">
            <Container>
                <Row classnames={`row align-items-center justify-content-around flex-lg-row-reverse`}>
                    <Column classnames={`col-md-9 col-lg-6 col-xl-5 mb-4 mb-lg-0 pl-lg-5 pl-xl-0`}>
                        <div data-aos="fade-in" data-aos-offset="50">
                            <h2 className="h1 text-center text-lg-left">what we think?</h2>
                            <div className="d-flex flex-wrap justify-content-center justify-content-lg-start">
                                <StatementBlock
                                    topic={`Plan Designed Around You`}
                                    statement={`Everyone especially you, should have a payment plan specially designed for you.
                                                    No more going broke to afford that furniture`}
                                />
                                <StatementBlock
                                    topic={`Highly Flexible`}
                                    statement={`With ${Company().name}, we deliver to your door step and you spread the payments over months.
                                                So flexible, you can do more with your money`}
                                />
                                <StatementBlock
                                    topic={`Checkout Anywhere`}
                                    statement={`Browse your favorite stores and checkout with <i>${Company().name}</i> to instantly create an account.
                                                Grow a good score on your Account to unlock more features.`}
                                />
                            </div>
                        </div>
                    </Column>
                    <Column classnames={`col-lg col-xl-6`}>
                        <DescriptionImage location={`assets/img/mobile-app/mobile-app-4.png`} alt={`Screen shot`}/>
                    </Column>
                </Row>
            </Container>
        </Section>
        <Section classnames={`pb-0 pt-0`}>
            <Container>
                <Row classnames={`row section-title justify-content-center text-center text-white`}>
                    <Column classnames={`col-md-9 col-lg-8 col-xl-7`}>
                        <a type="button" className="btn btn-lg btn-danger" onClick={e => { e.preventDefault(); routeTo(GetStartedClientRoute) }}>
                            Sign Up
                        </a>
                    </Column>
                </Row>
            </Container>
        </Section>
    </React.Fragment>
};

export default Home;

