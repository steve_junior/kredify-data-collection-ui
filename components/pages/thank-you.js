import React from 'react'
import {RegularNavigation} from "../Navbars";
import {CompanyMoto, Container, Section} from "../Section";
import {Column, Row} from "../Footer";


export const ThankYou = (props) => {

    return <React.Fragment>

        <RegularNavigation />

        <Section classnames={`o-hidden text-dark-green pb-0`}>
            <Container classnames={`container pb-5`}>
                <Row classnames={`row justify-content-center text-center`}>
                    <Column classnames={`col-md-9 col-lg-8 col-xl-7`}>
                        <h1 className="display-4">Welcome {props.name}</h1>
                        <p className="lead mb-0">We are excited to have you on-board, yay! Our team member will be in touch with you shortly. Thank You <strong><i>{props.name}</i></strong></p>
                    </Column>
                </Row>
            </Container>
        </Section>

        <CompanyMoto />

    </React.Fragment>
}