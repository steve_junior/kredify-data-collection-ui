import React from 'react'
import {RegularNavigation} from "../Navbars";
import {CompanyMoto, Container, Section} from "../Section";
import {Column, Row} from "../Footer";
import {Company, routeTo} from "../../src/helpers";
import {GetStartedClientRoute} from "../../src/custom-routes";


const AboutUs = (props) => {
    return <React.Fragment>
        <RegularNavigation />

        <Section classnames={`bg-primary-3 o-hidden text-white pb-0`}>
            <Container classnames={`container pb-5`}>
                <Row classnames={`row justify-content-center text-center`}>
                    <Column classnames={`col-md-9 col-lg-8 col-xl-7`}>
                        <h1 className="display-4">Our Mission</h1>
                        <p className="lead mb-0">{Company().name} is a smart solution for everyone to obtain merchandise they need now and pay in the most convenient way. Our mission is to close the gap between merchants and shoppers by providing both with more power to do more.</p>
                    </Column>
                </Row>
            </Container>
        </Section>

        <Section classnames={`o-hidden text-dark-green pb-0`}>
            <Container classnames={`container pb-5`}>
                <Row classnames={`row justify-content-center text-center`}>
                    <Column classnames={`col-md-9 col-lg-8 col-xl-7`}>
                        <h1 className="display-4">Who We Are</h1>
                        <p className="lead mb-0">A dedicated and competent customer-centric team with a mission to empower shoppers to reach greater heights</p>
                    </Column>
                </Row>
            </Container>
        </Section>

        <CompanyMoto />

        <Section classnames={`pb-0 pt-0 bg-primary-3 text-white`}>
            <Container>
                <Row classnames={`row section-title justify-content-center text-center text-white`}>
                    <Column classnames={`col-md-9 col-lg-8 col-xl-7`}>
                        <a type="button" className="btn btn-lg btn-outline-light" onClick={e => { e.preventDefault(); routeTo(GetStartedClientRoute) }}>
                            Sign Up To Join {Company().name}
                        </a>
                    </Column>
                </Row>
            </Container>
        </Section>

    </React.Fragment>
}
export default AboutUs