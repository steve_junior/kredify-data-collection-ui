import React, {useContext, useState} from 'react'
import { Container, Section} from "../Section";
import {Column, Row} from "../Footer";
import {Company, isDefined, routeTo} from "../../src/helpers";
import ConfigContext from "../Contexts/ConfigContext";
import {onboard_client, onboard_merchant} from "../../src/http-service";
import {Select} from "../Reusables";
import {GetStartedClientRoute, GetStartedMerchantRoute, WelcomeAboardRoute} from "../../src/custom-routes";
import {RegularNavigation} from "../Navbars";



const ClientForm = (props) => {

    const {income}                      = useContext(ConfigContext);
    const income_streams                = income.streams;
    const income_ranges                 = income.ranges;

    const [name, setName]                = useState("");
    const [email, setEmail]              = useState("");
    const [phone, setPhone]              = useState("");
    const [company_name, setCompanyName] = useState("");
    const [narration, setNarration]      = useState("");
    const [income_stream, setIncomeStream] = useState("");
    const [income_range, setIncomeRange] = useState("");


    async function submit(e) {
        e.preventDefault();
        const payload = {
            name,
            email,
            phone,
            company_name,
            narration,
            income_stream,
            income_range
        };

        const response = await onboard_client(payload);
        if(isDefined(response)){
            if(response.success){
                document.getElementById('data-on-board-client-success-message').classList.remove('d-none');
                document.getElementById('data-on-board-client-submit-form').disable = true;
                setTimeout(() => {
                    routeTo(WelcomeAboardRoute + '?name=' + name);
                }, 1000);
            }
        }
    }

    return <form className="card card-body shadow" onSubmit={submit}>
        <div className="form-group">
            <label htmlFor="client-interest">Narrate your interest</label>
            <textarea name="narration"
                      value={narration}
                      id="client-interest"
                      rows="4"
                      className="form-control"
                      required
                      placeholder={`I'd wish to renew my 2-year rent at East Legon and get an iPhone X without going broke.`}
                      onChange={e => { setNarration(e.target.value) }}
            >
                    </textarea>
            <div className="invalid-feedback">
                Please tell us a bit more about your requirements.
            </div>
        </div>

        <div className="form-row">
            <div className="col-sm-6">
                <div className="form-group">
                    <label htmlFor="client-top-income-stream">Top Income Stream</label>
                    <div className="position-relative">
                        <Select options={income_streams}
                                required id={`client-top-income-stream`}
                                name={`income_stream`}
                                classnames={`custom-select`}
                                value={income_stream}
                                onChange={e => {setIncomeStream(e.target.value)}}
                        />
                        <img src={`assets/img/icons/interface/icon-caret-down.svg`}
                             alt="Arrow Down" className="icon icon-sm"/>
                        <div className="invalid-feedback">
                            Please select an income stream.
                        </div>
                    </div>
                    {/*<small>Required to evaluate your account access limit</small>*/}
                </div>
            </div>
            <div className="col-sm-6">
                <div className="form-group">
                    <label htmlFor="client-income-range">Income Range</label>
                    <div className="position-relative">
                        <Select classnames={`custom-select`} id={`client-income-range`}
                                name={`income_range`}
                                value={income_range}
                                options={income_ranges}
                                onChange={e => { setIncomeRange(e.target.value) }}
                        />
                        <img src={`assets/img/icons/interface/icon-caret-down.svg`}
                             alt="Arrow Down" className="icon icon-sm"/>
                        <div className="invalid-feedback">
                            Please pick your income range.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div className="form-row">
            <div className="col-sm-6">
                <div className="form-group">
                    <label htmlFor="client-name">Name</label>
                    <input type="text" name="name" value={name} className="form-control"
                           id="client-name" required onChange={e => { setName(e.target.value) }}/>
                    <div className="invalid-feedback">
                        Please type your name.
                    </div>
                </div>
            </div>
            <div className="col-sm-6">
                <div className="form-group">
                    <label htmlFor="client-email">Email Address</label>
                    <input type="email" name="email" value={email} className="form-control"
                           id="client-email" placeholder="you@website.com" required onChange={ e => { setEmail(e.target.value) }}/>
                    <div className="invalid-feedback">
                        Please provide your email address.
                    </div>
                </div>
            </div>
        </div>
        <div className="form-row">
            <div className="col-sm-6">
                <div className="form-group">
                    <label htmlFor="client-phone">Phone</label>
                    <input type="tel" name="phone" value={phone} className="form-control"
                           id="client-phone"
                           required placeholder={`012 345 6789`}
                           onChange={ e => { setPhone(e.target.value) }}/>
                    <small>So we can reach you quickly</small>
                    <div className="invalid-feedback">
                        Please provide your phone number.
                    </div>
                </div>
            </div>
            <div className="col-sm-6">
                <div className="form-group">
                    <label htmlFor="client-company">Company</label>
                    <input type="text" name="company_name" value={company_name} className="form-control"
                           id="client-company" required
                           onChange={ e => { setCompanyName(e.target.value) }}/>
                </div>
            </div>
        </div>


        <div className="d-flex flex-column flex-wrap flex-md-row justify-content-between align-items-center">
            <div id={`data-on-board-client-success-message`} className="d-none alert alert-success w-100 my-md-3" role="alert">
                Thanks, a member of our team will be in touch shortly.
            </div>
            <div className="d-none alert alert-danger w-100 my-md-3" role="alert" id={`data-error-message`}>
                Please fill all fields correctly.
            </div>
            <button id={`data-on-board-client-submit-form`} className="btn btn-primary-3 btn-loading" type="submit"
                    data-loading-text="Sending">
                <span>Done</span>
            </button>
        </div>
        <div className="w-100 my-md-5 text-danger cursor-pointer font-18 text-center">
            <a onClick={e => { e.preventDefault(); routeTo(GetStartedMerchantRoute)}}>Sign Up As Merchant, Click here</a>
        </div>
    </form>
};

export const OnboardClient = (props) => {
    return <React.Fragment>

            <RegularNavigation />

            <div className="bg-primary-3 p-0 o-hidden" data-overlay>
                <Section classnames={`pb-0`}>
                    <Container>
                        <Row classnames={`row section-title justify-content-center text-center text-white`}>
                            <Column classnames={`col-md-9 col-lg-8 col-xl-7`}>
                                <h1 className={`d-none`}>Getting Started with {Company().name}</h1>
                                <h2 className="display-3">Let's get you started</h2>
                                <span className="lead">This should take less than 2 mins</span>
                            </Column>
                        </Row>
                        <Row classnames={`row justify-content-center`}>
                            <Column classnames={`col-xl-8 col-lg-9 col-md-11`}>
                                <ClientForm />
                            </Column>
                        </Row>
                    </Container>
                    <div className="divider divider-bottom bg-white"/>
                </Section>
            </div>
        </React.Fragment>
};


const MerchantForm = (props) => {

    const [category, setCategory]        = useState("");
    const [location, setLocation]        = useState("");
    const [website, setWebsite]          = useState("");
    const [email, setEmail]              = useState("");
    const [phone, setPhone]              = useState("");
    const [company_name, setCompanyName] = useState("");
    const [narration, setNarration]      = useState("");


    async function submit(e) {
        e.preventDefault();

        const payload = {
            email,
            phone,
            company_name,
            narration,
            category,
            location_or_website : location + ' | ' + website
        };

        const response = await onboard_merchant(payload);
        if(isDefined(response)){
            if(response.success){
                document.getElementById('data-on-board-merchant-success-message').classList.remove('d-none');
                document.getElementById('data-on-board-merchant-submit-form').disable = true;
                setTimeout(() => {
                    routeTo(WelcomeAboardRoute + '?name=' + company_name);
                }, 1000);
            }
        }
    }

    return <form className="card card-body shadow" onSubmit={submit}>
        <div className="form-row">
            <div className="col-sm-6">
                <div className="form-group">
                    <label htmlFor="merchant-company">Company</label>
                    <input type="text" name="company_name" value={company_name} className="form-control"
                           id="merchant-company" required
                           onChange={ e => { setCompanyName(e.target.value) }}/>
                </div>
            </div>
            <div className="col-sm-6">
                <div className="form-group">
                    <label htmlFor="merchant-category">Product Line</label>
                    <input type="text" name="category" value={category} className="form-control"
                           id="merchant-category" placeholder="Electronics, Real Estate or General" required onChange={ e => { setCategory(e.target.value) }}/>
                    <div className="invalid-feedback">
                        Please provide your product line of business.
                    </div>
                </div>
            </div>
        </div>
        <div className="form-row">
            <div className="col-sm-6">
                <div className="form-group">
                    <label htmlFor="merchant-location">Location</label>
                    <input type="text" name="location" value={location} className="form-control"
                           id="merchant-location" placeholder="22 Avenue, North Legon" onChange={ e => { setLocation(e.target.value) }}/>
                    <div className="invalid-feedback">
                        Please provide your physical address .
                    </div>
                </div>
            </div>
            <div className="col-sm-6">
                <div className="form-group">
                    <label htmlFor="merchant-website">Website</label>
                    <input type={`text`} name="website" value={website} className="form-control"
                           id="merchant-website" placeholder="www.no1beautyproducts.com"  onChange={ e => { setWebsite(e.target.value) }}/>
                    <div className="invalid-feedback">
                        Please provide your website will do.
                    </div>
                </div>
            </div>
        </div>
        <div className={`form-row`}>
            <div className="col-sm-6">
                <div className="form-group">
                    <label htmlFor="merchant-email">Email</label>
                    <input type="email" name="email" value={email} className="form-control"
                           id="merchant-email" required onChange={e => { setEmail(e.target.value) }}/>
                    <div className="invalid-feedback">
                        Please type your email address.
                    </div>
                </div>
            </div>
            <div className="col-sm-6">
                <div className="form-group">
                    <label htmlFor="merchant-phone">Phone</label>
                    <input type="tel" name="phone" value={phone} className="form-control"
                        id="merchant-phone"
                        required placeholder={`012 345 6789`}
                        onChange={ e => { setPhone(e.target.value) }}/>
                    <small>So we can reach you quickly</small>
                    <div className="invalid-feedback">
                        Please provide your phone number.
                    </div>
                </div>
            </div>
        </div>
        <div className="form-group">
            <label htmlFor="merchant-interest">Narrate your interest</label>
            <textarea name="narration"
                      value={narration}
                      id="merchant-interest"
                      rows="4"
                      className="form-control"
                      required
                      placeholder={`I'd like to on-board to offer my services and product lines to customers of ${Company().name} and get paid under my terms.`}
                      onChange={e => { setNarration(e.target.value) }}
            >
                    </textarea>
            <div className="invalid-feedback">
                Please tell us a bit more about your requirements.
            </div>
        </div>


        <div className="d-flex flex-column flex-wrap flex-md-row justify-content-between align-items-center">
            <div id={`data-on-board-merchant-success-message`} className="d-none alert alert-success w-100 my-md-3" role="alert">
                Thanks, a member of our team will be in touch shortly.
            </div>
            <div className="d-none alert alert-danger w-100 my-md-3" role="alert" id={`data-error-message`}>
                Please fill all fields correctly.
            </div>
            <button id={`data-on-board-merchant-submit-form`} className="btn btn-primary-3 btn-loading" type="submit"
                    data-loading-text="Sending">
                <span>Done</span>
            </button>
        </div>
        <div className="w-100 my-md-5 text-danger cursor-pointer font-25 text-center">
            <a onClick={e => { e.preventDefault(); routeTo(GetStartedClientRoute)}}>Sign Up As Shopper Only? Click Here</a>
        </div>
    </form>
};

export const OnboardMerchant = (props) => {

    return <React.Fragment>

            <RegularNavigation />

            <div className="bg-primary-3 p-0 o-hidden" data-overlay>
                <Section classnames={`pb-0`}>
                    <Container>
                        <Row classnames={`row section-title justify-content-center text-center text-white`}>
                            <Column classnames={`col-md-9 col-lg-8 col-xl-7`}>
                                <h1 className={`d-none`}>Getting Started with {Company().name} as Merchant</h1>
                                <h2 className="display-3">Sell with {Company().name}</h2>
                                <span className="lead">This should take less than 2 mins</span>
                            </Column>
                        </Row>
                        <Row classnames={`row justify-content-center`}>
                            <Column classnames={`col-xl-8 col-lg-9 col-md-11`}>
                                <MerchantForm />
                            </Column>
                        </Row>
                    </Container>
                    <div className="divider divider-bottom bg-white"/>
                </Section>
            </div>
        </React.Fragment>
};