import React from 'react';

const Modal = (props) => {
    return <React.Fragment>
        <div className={`modal ${props.appearanceClasses}`} id={props.id} tabIndex="-1" role="dialog" aria-hidden="true">
            {props.children}
        </div>
    </React.Fragment>
};

Modal.defaultProps = {
    appearanceClasses : "fade"
};

