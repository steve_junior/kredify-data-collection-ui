import React from 'react'
import {Container, Nav, NavContainer} from "./Section";
import {Image} from "./Reusables";
import {HomeRoute} from "../src/custom-routes";

export const RegularNavigation = (props) => {

    return <NavContainer>
            <Nav classnames={`navbar navbar-expand-lg bg-transparent`}  data-overlay data-sticky="top">
                <Container>
                    <a href={props.routeTo} className="navbar-brand navbar-brand-dynamic-color fade-page">
                        <Image alt="Kredify Logo"  src={`/assets/img/custom/logos/1080x566.png`} />
                    </a>
                </Container>
            </Nav>
        </NavContainer>
}
RegularNavigation.defaultProps = {
    routeTo : HomeRoute
}
