import React from 'react'
import {withRouter} from 'next/router'
import {HeadTag} from "./HeadTag";
import {BodyTag, LoadingAnimation} from "./Reusables";
import {TopAlertBanner} from "./TopAlertBanner";
import { out } from "../src/helpers";
import JsScripts from "./JsScripts";
const tawkTo = require("tawkto-react");


const Views = (props) =>{
    return props.children;
};


class Wrapper extends React.Component {
    constructor(props){
        super(props);

        this.handleLoadEvent        = this.handleLoadEvent.bind(this);
        this.handleLoadingAnimation = this.handleLoadingAnimation.bind(this);
    }

    componentDidMount(){
        out('Wrapper componentDidMount');
        window.addEventListener("load", this.handleLoadEvent);

        if(process.env.ENV === 'production'){
            // initialise Tawk.to plugin
            tawkTo(process.env.TAWK_TO_PROPERTY_ID)
        }
    }

    componentWillUnmount(){
        out('Wrapper componentWillUnmount');
        window.removeEventListener("load", this.handleLoadEvent);
    }

    handleLoadingAnimation(){
        let loading = document.querySelector(".body");
        console.log(loading);
        loading.classList.add("loaded");
    }

    handleLoadEvent(){
        // this.handleLoadingAnimation();
    }

    render(){
        out('Wrapper render');
        return <React.Fragment>
                <HeadTag />
                    <BodyTag>
                        <LoadingAnimation />
                        <TopAlertBanner
                            appearance={this.props.displayTopBanner}
                            message={`With the <strong>CoVid 19</strong> outbreak, it is imperative to stay safe with your family at home`}
                            reference="https://www.who.int/emergencies/diseases/novel-coronavirus-2019/advice-for-public"
                            actionMessage="WHO Guidelines"
                            closable={true}
                        />
                        <Views {...this.props} />
                        <JsScripts />
                    </BodyTag>
            </React.Fragment>
    }
}

Wrapper.defaultProps = {
    displayTopBanner : true,
};

export default withRouter(Wrapper)

