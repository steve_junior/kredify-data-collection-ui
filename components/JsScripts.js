import React from 'react';

const JsScripts = (props) => {
    return <React.Fragment>
        {/*Required vendor scripts (Do not remove) */}
        <script type="text/javascript" src={`assets/js/jquery.min.js`} />
        <script type="text/javascript" src={`assets/js/popper.min.js`} />
        <script type="text/javascript" src={`assets/js/bootstrap.js`} />

        {/*Optional Vendor Scripts (Remove the plugin script here and comment initializer script out of index.js if site does not use that feature) */}

        {/*AOS (Animate On Scroll - animates elements into view while scrolling down)*/}
        <script type="text/javascript" src={`assets/js/aos.js`} />
        {/*Clipboard (copies content from browser into OS clipboard)*/}
        <script type="text/javascript" src={`assets/js/clipboard.min.js`} />
        {/*Fancybox (handles image and video lightbox and galleries)*/}
        {/*<script type="text/javascript" src="assets/js/jquery.fancybox.min.js"></script>*/}
        {/*Flatpickr (calendar/date/time picker UI)*/}
        {/*Flatpickr (required by theme.js)*/}
        <script type="text/javascript" src={`assets/js/flatpickr.min.js`} />
        {/*Flickity (handles touch enabled carousels and sliders)*/}
        <script type="text/javascript" src={`assets/js/flickity.pkgd.min.js`} />
        {/*Ion rangeSlider (flexible and pretty range slider elements)*/}
        <script type="text/javascript" src={`assets/js/ion.rangeSlider.min.js`} />
        {/*Isotope (masonry layouts and filtering)*/}
        <script type="text/javascript" src={`assets/js/isotope.pkgd.min.js`} />
        {/*jarallax (parallax effect and video backgrounds)*/}
        <script type="text/javascript" src={`assets/js/jarallax.min.js`} />
        <script type="text/javascript" src={`assets/js/jarallax-video.min.js`} />
        <script type="text/javascript" src={`assets/js/jarallax-element.min.js`}/>
        {/*jQuery Countdown (displays countdown text to a specified date)*/}
        {/*jQuery Countdown (required by theme.js)*/}
        <script type="text/javascript" src={`assets/js/jquery.countdown.min.js`} />
        {/*jQuery smartWizard facilitates steppable wizard content*/}
        <script type="text/javascript" src={`assets/js/jquery.smartWizard.min.js`} />
        {/*Plyr (unified player for Video, Audio, Vimeo and Youtube)*/}
        <script type="text/javascript" src={`assets/js/plyr.polyfilled.min.js`} />
        {/*Prism (displays formatted code boxes)*/}
        <script type="text/javascript" src={`assets/js/prism.js`} />
        {/*// ScrollMonitor (manages events for elements scrolling in and out of view)*/}
        <script type="text/javascript" src={`assets/js/scrollMonitor.js`} />
        {/*// Smooth scroll (animation to links in-page*/}
        <script type="text/javascript" src={`assets/js/smooth-scroll.polyfills.min.js`} />
        {/*// SVGInjector (replaces img tags with SVG code to allow easy inclusion of SVGs with the benefit of inheriting colors and styles*/}
        <script type="text/javascript" src={`assets/js/svg-injector.umd.production.js`} />
        {/*// TwitterFetcher (displays a feed of tweets from a specified account*/}
        <script type="text/javascript" src={`assets/js/twitterFetcher_min.js`} />
        {/*Typed text (animated typing effect*/}
        <script type="text/javascript" src={`assets/js/typed.min.js`} />
        {/*Required theme scripts (Do not remove)*/}
        <script type="text/javascript" src={`assets/js/theme.js`} />
    </React.Fragment>
};

export default JsScripts;