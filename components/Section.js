import React from 'react';
import {Company, isDefined} from "../src/helpers";
import {SetInnerHtml} from "./Reusables";
import {Column, Row} from "./Footer";

export const Section = (props) => {
  return <section className={props.classnames}>
            {props.children}
        </section>
};

export const StatementBlock = (props) => {

    return   <React.Fragment>
        <div className="my-4">
            <div className="d-flex">
                {
                    isDefined(props.image) ?
                        <div className="mr-3 mr-md-4">
                            <img src={props.image} alt={props.imageAlt}
                                 className={props.imageStyle} data-inject-svg />
                        </div> : null
                }
                <div>
                    <h5>{props.topic}</h5>
                    <div>
                        <SetInnerHtml html={props.statement} />
                    </div>
                </div>
            </div>
        </div>
    </React.Fragment>
};

StatementBlock.defaultProps = {
    imageStyle : "icon bg-primary",

};

export const DescriptionImage = (props) => {
    return  <React.Fragment>
        <div className="row justify-content-center" data-jarallax-element="-50">
            <div className="col-10 col-sm-8 col-md-6 col-lg-8 col-xl-6">
                <img className="img-fluid position-relative" src={props.location} alt={props.alt} />
                <div className="h-75 w-75 position-absolute bottom right d-none d-lg-block" data-jarallax-element="-50">
                    <div className="blob blob-4 w-100 h-100 bg-success opacity-90" />
                </div>
            </div>
        </div>
    </React.Fragment>
};

export const Container = (props) => {
    return <React.Fragment>
        <div className={props.classnames}>
            {props.children}
        </div>
    </React.Fragment>
};

Container.defaultProps = {
    classnames : "container"
};

export const NavContainer = (props) => {
    return <div className={props.classnames}>
        {props.children}
    </div>
}
NavContainer.defaultProps = {
    classnames : "nav-container"
}

export const Nav = (props) => {
    return <nav className={props.classnames}>
        {props.children}
    </nav>
}

export const ContactBlock = () => {
    return <Section classnames={`p-3`}>
        <Container>
            <Row classnames={`row justify-content-center text-center`}>
                <Column classnames={`col-md-auto`}>
                    <div className="px-md-4 px-lg-5 mb-5 mb-md-0">
                        <h5>Email us</h5>
                        <a href={`mailto: ${Company().support.email}`} className="lead text-dark-green">{Company().support.email}</a>
                    </div>
                </Column>
                <Column classnames={`col-md-auto`}>
                    <div className={`px-md-4 px-lg-5 mb-5 mb-md-0`}>
                        <h5>Location</h5>
                        {Company().address}
                    </div>
                </Column>
                {/*<Column classnames={`col-md-auto`}>*/}
                {/*<div className="px-md-4 px-lg-5 mb-5 mb-md-0">*/}
                {/*<h5>Call any time</h5>*/}
                {/*<a href={`tel: +${Company().support.contact}`} className="lead">{Company().support.contact}</a>*/}
                {/*</div>*/}
                {/*</Column>*/}
            </Row>
        </Container>
    </Section>
}

export const LineBreak = (props) => {
    return <div className={props.classnames}/>
};

LineBreak.defaultProps = {
    classnames : 'divider'
}

export const CompanyMoto = (props) => {
    return <Section classnames={`bg-primary-3 text-white pt-0`}>
            <LineBreak classnames={`divider divider-top transform-flip-x`} />
            <Container>
                <Row classnames={`row section-title justify-content-center text-center`}>
                    <Column classnames={`col-md-9 col-lg-8 col-xl-7`}>
                        <div className="lead font-italic text-lowercase font-weight-bolder">you need it...just {Company().name} it</div>
                        <h3 className="display-4">Safely and Reliably</h3>
                    </Column>
                </Row>
            </Container>
        </Section>
}
