import React from 'react'
import {useRouter} from 'next/router';
import Head from 'next/head'
import {Company, getPageTitle} from "../src/helpers";



export const HeadTag = (props) => {
    return (
        <React.Fragment>
            <Head>
                <meta charSet="utf-8" />
                <title>{`${Company().name + ': ' + getPageTitle(useRouter().route)} - Shop now, Pay later. Safely and Reliably`}</title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <meta
                    name="description"
                    content={process.env.SITE_DESCRIPTION}
                />
                <meta name="keywords" content={process.env.SITE_KEYWORDS}/>
                <meta name="robots" content="noarchive" />
            </Head>
                {/* Begin loading animation */}
                <CSSLink reference="css/loaders/loader-pulse.css"/>
                {/* End loading animation */}

                <CSSLink reference="css/theme.css"/>

                <CSSLink reference="https://fonts.googleapis.com/css?family=Nunito:400,400i,600,700&display=swap"/>

                <CSSLink reference={`img/custom/favicons/apple-touch-icon.png`}  property={{sizes : "180x180", key : "apple-touch-icon"}} rel={`apple-touch-icon`} />
                <CSSLink reference={`img/custom/favicons/favicon-32x32.png`}     property={{sizes : "32x32", type : "image/png",  key : "favicon-32x32-icon"}} rel={`icon`} />
                <CSSLink reference={`img/custom/favicons/favicon-16x16.png`}     property={{sizes : "16x16", type : "image/png", key : "favicon-16x16-icon"}} rel={`icon`} />
                <CSSLink reference={`img/custom/favicons/site.webmanifest`}      rel={`manifest`}  property={{key : "site-webmanifest"}}/>
                <CSSLink reference={`img/custom/favicons/safari-pinned-tab.svg`} rel={`mask-icon`} property={{color : "#5bbad5", key: "mask-icon"}} />
            <Head>
                <meta name="msapplication-TileColor" content="#da532c"/>
                <meta name="theme-color" content="#ffffff" />
            </Head>
        </React.Fragment>
    )
};

HeadTag.defaultProps = {
    pageTitle : ''
};

export const CSSLink = ({reference, media = "all", type = "text/css", property = null, rel = "stylesheet"}) => {

    function hasHttp(path){
        return path.substring(0, 4) === 'http';
    }

    const base_path = hasHttp(reference) ?  '' : "/assets/";

    return <Head><link media={media} rel={rel} type={type} href={base_path + reference} {...property}/></Head>
};

export const JsLink = ({reference, property = null}) => {

    function hasHttp(path){
        return path.substring(0, 4) === 'http';
    }

    const base_path = hasHttp(reference) ?  '' : "/assets/";

    return <Head><script src={base_path + reference} {...property}/></Head>
};