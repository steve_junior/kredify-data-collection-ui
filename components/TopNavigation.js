import React from 'react'

export const TopNavigationBar = (props) => {
    return <React.Fragment>
        <div className="navbar-container bg-primary-3">
            <nav className="navbar navbar-expand-lg navbar-dark" data-sticky="top">
                <div className="container">
                    <a
                        className="navbar-brand navbar-brand-dynamic-color fade-page"
                        href=""
                    >
                        <img
                            alt="PayLater"
                            data-inject-svg
                            src="" //set logo here
                        />
                    </a>
                    {/*<div className="d-flex align-items-center order-lg-3">*/}
                        {/*<a*/}
                            {/*href="#"*/}
                            {/*className="btn btn-primary ml-lg-4 mr-3 mr-md-4 mr-lg-0 d-none d-sm-block order-lg-3"*/}
                        {/*>*/}
                            {/*PayLaters*/}
                        {/*</a>*/}
                        {/*<button*/}
                            {/*aria-expanded="false"*/}
                            {/*aria-label="Toggle navigation"*/}
                            {/*className="navbar-toggler"*/}
                            {/*data-target=".navbar-collapse"*/}
                            {/*data-toggle="collapse"*/}
                            {/*type="button"*/}
                        {/*>*/}
                            {/*<img*/}
                                {/*alt="Navbar Toggler Open Icon"*/}
                                {/*className="navbar-toggler-open icon icon-sm"*/}
                                {/*data-inject-svg*/}
                                {/*src="assets/img/icons/interface/icon-menu.svg"*/}
                            {/*/>*/}
                            {/*<img*/}
                                {/*alt="Navbar Toggler Close Icon"*/}
                                {/*className="navbar-toggler-close icon icon-sm"*/}
                                {/*data-inject-svg*/}
                                {/*src="assets/img/icons/interface/icon-x.svg"*/}
                            {/*/>*/}
                        {/*</button>*/}
                    {/*</div>*/}
                   <NavigationLinks />
                </div>
            </nav>
        </div>
    </React.Fragment>
};

export const NavigationLinks = (props) => {
    return  (
        <React.Fragment>
            {props.display ? <div
                className="collapse navbar-collapse order-3 order-lg-2 justify-content-lg-end"
                id="navigation-menu">
                <ul className="navbar-nav my-3 my-lg-0">
                    <NavLink linkName="Demo"/>
                </ul>
            </div> : null }
        </React.Fragment>
    )
};

NavigationLinks.defaultProps = {
  display : false
};

export const NavLink = (props) => {
   return  <li className="nav-item">
       <div className="dropdown">
           <a
               aria-expanded="false"
               aria-haspopup="true"
               className="dropdown-toggle nav-link nav-item arrow-bottom"
               data-toggle="dropdown-grid"
               href="#"
               role="button"
           >
               {props.linkName}
           </a>
           <div className="row dropdown-menu">
               <div className="col-auto" data-dropdown-content>
                   <div className="dropdown-grid-menu">
                       <NavSubLink reference="/" subLinkName="Overview"/>
                   </div>
               </div>
           </div>
       </div>
   </li>
};

export const NavSubLink = (props) => {
  return  <a href={props.reference} className="dropdown-item fade-page">
            {props.subLinkName}
          </a>
};

