import React, {useState} from 'react'
import {useRouter} from 'next/router';
import {subscribe_to_news_letter} from "../src/http-service";
import {Company, isDefined} from "../src/helpers";
import {AboutRoute, GetStartedClientRoute, GetStartedMerchantRoute} from "../src/custom-routes";



export const Footer = (props) => {
  return (
      <footer className="bg-primary-3 text-white links-white pb-4 footer-1">
          <div className="container">
              <Row>
                  <Column classnames="col-xl-auto mr-xl-5 col-md-3 mb-4 mb-md-0">
                      {/*<h5>Partners</h5>*/}
                      <ul className="nav flex-row flex-md-column">
                          <li className="nav-item mr-3 mr-md-0">
                              <a href={AboutRoute} className="nav-link px-0 py-2 text-size-20">
                                  About
                              </a>
                              <a href={GetStartedMerchantRoute} className="nav-link px-0 py-2 text-size-20">
                                  I'm a merchant
                              </a>
                              <a href={GetStartedClientRoute} className="nav-link px-0 py-2 text-size-20">
                                  Interested in shopping
                              </a>
                          </li>
                      </ul>
                  </Column>
                  {/*<Column classnames="col-xl-auto mr-xl-5 col-md-3">*/}
                      {/*<h5>About</h5>*/}
                      {/*<ul className="nav flex-row flex-md-column">*/}
                          {/*<li className="nav-item mr-3 mr-md-0">*/}
                              {/*<a*/}
                                  {/*href=""*/}
                                  {/*className="nav-link fade-page px-0 py-2"*/}
                              {/*>*/}
                                  {/*Company*/}
                              {/*</a>*/}
                          {/*</li>*/}
                      {/*</ul>*/}
                  {/*</Column>*/}
                  <Column classnames="col-xl-auto mt-2 mt-md-5 mt-lg-0 order-lg-3 order-xl-4">
                      <NewsLetter />
                  </Column>
              </Row>

              <Row>
                  <Column>
                      <hr />
                  </Column>
              </Row>

              <div className="row flex-column flex-lg-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
                  <div className="col-auto">
                      <div className="d-flex flex-column flex-sm-row align-items-center text-small">
                          <div className="text-muted">
                              Copyright © 2020 <a href="#">{Company().name}</a>
                          </div>
                      </div>
                  </div>
                  {/*<div className="col-auto mt-3 mt-lg-0">*/}
                      {/*<ul className="list-unstyled d-flex mb-0">*/}
                          {/*<li className="mx-3">*/}
                              {/*<a href="#" className="hover-fade-out">*/}
                                  {/*<img*/}
                                      {/*src="/assets/img/icons/social/dribbble.svg"*/}
                                      {/*alt="Dribbble"*/}
                                      {/*className="icon icon-xs bg-white"*/}
                                      {/*data-inject-svg*/}
                                  {/*/>*/}
                              {/*</a>*/}
                          {/*</li>*/}
                          {/*<li className="mx-3">*/}
                              {/*<a href="#" className="hover-fade-out">*/}
                                  {/*<img*/}
                                      {/*src="/assets/img/icons/social/twitter.svg"*/}
                                      {/*alt="Twitter"*/}
                                      {/*className="icon icon-xs bg-white"*/}
                                      {/*data-inject-svg*/}
                                  {/*/>*/}
                              {/*</a>*/}
                          {/*</li>*/}
                          {/*<li className="mx-3">*/}
                              {/*<a href="#" className="hover-fade-out">*/}
                                  {/*<img*/}
                                      {/*src="/assets/img/icons/social/github.svg"*/}
                                      {/*alt="Github"*/}
                                      {/*className="icon icon-xs bg-white"*/}
                                      {/*data-inject-svg*/}
                                  {/*/>*/}
                              {/*</a>*/}
                          {/*</li>*/}
                          {/*<li className="mx-3">*/}
                              {/*<a href="#" className="hover-fade-out">*/}
                                  {/*<img*/}
                                      {/*src="/assets/img/icons/social/facebook.svg"*/}
                                      {/*alt="Facebook"*/}
                                      {/*className="icon icon-xs bg-white"*/}
                                      {/*data-inject-svg*/}
                                  {/*/>*/}
                              {/*</a>*/}
                          {/*</li>*/}
                          {/*<li className="mx-3">*/}
                              {/*<a href="#" className="hover-fade-out">*/}
                                  {/*<img*/}
                                      {/*src="assets/img/icons/social/google.svg"*/}
                                      {/*alt="Google"*/}
                                      {/*className="icon icon-xs bg-white"*/}
                                      {/*data-inject-svg*/}
                                  {/*/>*/}
                              {/*</a>*/}
                          {/*</li>*/}
                      {/*</ul>*/}
                  {/*</div>*/}
              </div>
          </div>
      </footer>
  )
};

export const NewsLetter = (props) => {
    const [email, setEmail] = useState("");
    const router = useRouter();

     async function submit(e){
        e.preventDefault();

        const response = await subscribe_to_news_letter({email});

        if(isDefined(response)){
            if(response.success){
                document.getElementById('data-subscribe-success-message').classList.remove('d-none');
                setTimeout(() => {
                   router.reload();
                }, 2000)
            }
        }
    }

    return (
        <React.Fragment>
            <h5 className={`text-uppercase`}>NewsLetters</h5>
            <div className="card card-body bg-white">
                <p>Sign up to {Company().name} news and get notified of new store alerts, updates and special offers.</p>
                <form data-form-email onSubmit={submit}>
                    <div className="d-flex flex-column flex-sm-row form-group">
                        <input
                            className="form-control mr-sm-2 mb-2 mb-sm-0 w-auto flex-grow-1"
                            name="email"
                            placeholder="Enter email"
                            type="email"
                            onChange={event => {setEmail(event.target.value)}}
                            value={email}
                            required
                        />
                        <button
                            type="submit"
                            className="btn btn-danger btn-loading"
                            data-loading-text="Sending"
                        >
                            <span>Subscribe</span>
                        </button>
                    </div>

                    <div
                        className="d-none alert alert-success w-100"
                        role="alert"
                        id={`data-subscribe-success-message`}
                    >
                        {`Thanks for subscribing.`}
                    </div>
                    <div
                        className="d-none alert alert-danger w-100"
                        role="alert"
                        data-error-message
                    >
                        Please fill all fields correctly.
                    </div>
                    <div className="text-small text-muted">
                        We will not share your email with anyone
                    </div>
                </form>
            </div>
        </React.Fragment>
    )
};

export const Column = (props) => {
    return <div className={props.classnames}>
        {props.children}
    </div>
};

Column.defaultProps = {
    classnames : 'col'
};

export const Row = (props) => {
    return <div className={props.classnames}>
        {props.children}
    </div>
};

Row.defaultProps = {
    classnames : 'row'
};