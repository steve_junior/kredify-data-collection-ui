import React from 'react';
import {isDefined} from "../src/helpers";
import {Image, SetInnerHtml} from "./Reusables";



export const TopAlertBanner = (props) => {
    return (
        <React.Fragment>
            <div className={`alert alert-dismissible d-none d-md-block py-4 py-md-3 px-0 mb-0 rounded-0 border-0 ` + props.backgroundColor}>
                <div className="container">
                    <div className="row no-gutters align-items-md-center justify-content-center">
                        <div className="col-lg-11 col-md d-flex flex-column flex-md-row align-items-md-center justify-content-lg-center">
                            <div className="mb-3 mb-md-0">
                                <SetInnerHtml html={props.message} />
                            </div>
                            {
                                isDefined(props.reference) ? <a className="btn btn-sm btn-light ml-md-3" target="_blank" href={props.reference}>{props.actionMessage}</a> : null
                            }
                        </div>
                        {props.closable ? <div className="col-auto position-absolute right">
                                            <button type="button" className="close p-0 position-relative" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">
                                                    <Image src={`assets/img/icons/interface/icon-x.svg`} alt="Close" classnames="icon icon-sm bg-white" property={{"data-inject-svg": true}} />
                                                </span>
                                            </button>
                                        </div> : null }
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
};

TopAlertBanner.defaultProps = {
    backgroundColor : "bg-danger text-white",
    actionMessage : "See more",
    closable     : false
};