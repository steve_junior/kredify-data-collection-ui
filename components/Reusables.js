import React from 'react'

export const LoadingAnimation = (props) => {
    return <div className="loader">
        <div className="loading-animation" />
    </div>
};

export const SetInnerHtml = (props) => {
    return <div dangerouslySetInnerHTML={{__html: props.html}} />
};

export const BodyTag = (props) => {
  return <div className={`body loaded`}>
          {props.children}
        </div>
};

export const Select = (props) => {
    return <select className={props.classnames} {...props} required>
        <Options {...props}/>
    </select>;
};

export const Options = (props) => {
    let container = [
        <option defaultValue key={-1} value="">Select Option</option>
    ];

    Object.entries(props.options).map((element, id)=> {
        if(element instanceof Array){
            container.push(<option value={element[0]} key={id}>{element[1]}</option>);
        }
    });

    return container;
};

export const Image = (props) => {
    return <img className={props.classnames} src={props.src} alt={props.alt} {...props.property}/>
};