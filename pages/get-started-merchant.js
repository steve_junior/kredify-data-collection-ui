import React from 'react'
import Wrapper from '../components/Wrapper'
import {OnboardMerchant} from "../components/pages/get-started";
import {ContactBlock} from "../components/Section";
import {Footer} from "../components/Footer";

const GetStartedMerchant = (props) => {
    return <Wrapper>
        <OnboardMerchant />
        <ContactBlock />
        <Footer />
    </Wrapper>
}
export default GetStartedMerchant