import React from 'react';
import Wrapper from "../components/Wrapper";
import Home from "../components/pages/home";
import {Footer} from "../components/Footer";


const Index = (props) => {

    return <Wrapper>
            <Home />
            <Footer />
        </Wrapper>
};

export default Index