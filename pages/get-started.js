import React from 'react'
import Wrapper from '../components/Wrapper'
import {OnboardClient} from "../components/pages/get-started";
import {Footer} from "../components/Footer";
import {ContactBlock} from "../components/Section";

const GetStarted = (props) => {
    return <Wrapper>
            <OnboardClient />
            <ContactBlock />
            <Footer />
        </Wrapper>
}
export default GetStarted