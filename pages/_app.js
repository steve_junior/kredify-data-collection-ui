import React from 'react';
import App from 'next/app';
import {withRouter} from 'next/router'
import {get_config_data} from "../src/http-service";
import {isDefined, isStrictlyDefined, out} from "../src/helpers";
import ConfigContext from "../components/Contexts/ConfigContext";
import * as ReactGA from 'react-ga';


class KredifyApp extends App {
    constructor(props){
        super();

        this.state = {
            config : {
                income : {
                    streams : [],
                    ranges : []
                }
            }
        };

        this.setConfig = this.setConfig.bind(this);
    }

    async setConfig(){
        const response = await get_config_data();
        if(isDefined(response)){
            const {...config} = response.data.config;
            this.setState({config})
        }
        else{
            console.log('Connect to API Server for config data')
        }
    }

   componentDidMount(){
        this.setConfig();

       if(process.env.ENV === 'production' || process.env.ENV === 'testing'){
           ReactGA.initialize(process.env.GA_TRACKING_ID);
           ReactGA.pageview(this.props.router.asPath);
       }
   }

    render() {

        const { Component, pageProps } = this.props;
        const configuration = this.state.config;

        if(isStrictlyDefined(configuration)){
            return (
                <ConfigContext.Provider value={{...configuration}}>
                    <Component {...pageProps} />
                </ConfigContext.Provider>
            )
        }
        return null
    }
}

export default withRouter(KredifyApp);