import React from 'react';
import {SiteRoutes} from "../src/custom-routes";

class GenerateSitemap extends React.Component {

    static sitemapXMLS() {

        let siteUrls = '';

       SiteRoutes.forEach(route => {
           siteUrls += `
                        <url>
                            <loc>${route.location}</loc>                           
                            <changefreq>${route.changeFrequency}</changefreq>
                            <priority>${route.priority}</priority>
                        </url>`;
       });

        return `<?xml version="1.0" encoding="UTF-8"?>
              <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">             
               ${siteUrls}
              </urlset>`;
    };

    /*
     * Uncomment this function whenever, your want to generate new sitemap
     */

    // static async getInitialProps({ req , res }) {
    //     res.setHeader("Content-Type", "text/xml");
    //     res.write(GenerateSitemap.sitemapXMLS());
    //     res.end();
    // }

    render() {
        return null
    }
}

export default GenerateSitemap