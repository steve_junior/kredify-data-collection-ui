import React from 'react'
import Wrapper from "../components/Wrapper";
import AboutUs from "../components/pages/about-us";
import {Footer} from "../components/Footer";

const About = () => {

    return <Wrapper>
        <AboutUs />
        <Footer />
    </Wrapper>
};

export default About