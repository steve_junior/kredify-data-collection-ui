import React, {useEffect} from 'react'
import {useRouter} from 'next/router';
import Wrapper from "../components/Wrapper";
import {Footer} from "../components/Footer";
import {ThankYou} from "../components/pages/thank-you";
import {getUrlParameter, routeTo} from "../src/helpers";
import {HomeRoute} from "../src/custom-routes";

const WelcomeAboard = () => {

    useEffect(() => {
        setTimeout(() => {
            routeTo(HomeRoute)
        }, 5000);
    }, []);

    return <Wrapper>
        <ThankYou name={getUrlParameter(useRouter().asPath, "name")} />
        <Footer />
    </Wrapper>
};

export default WelcomeAboard