export const HomeRoute                = '/';
export const GetStartedClientRoute    = '/get-started';
export const GetStartedMerchantRoute  = '/get-started-merchant';
export const AboutRoute               = '/about';
export const WelcomeAboardRoute       = '/welcome-aboard';


export const SiteRoutes  = [
    {
        name     : HomeRoute,
        location : process.env.PRODUCTION_APP_URL + HomeRoute,
        priority : 1.00,
        changeFrequency : 'daily'
    },
    {
        name     : GetStartedClientRoute,
        location : process.env.PRODUCTION_APP_URL + GetStartedClientRoute,
        priority : 1.00,
        changeFrequency : 'daily'
    },
    {
        name     : GetStartedMerchantRoute,
        location : process.env.PRODUCTION_APP_URL + GetStartedMerchantRoute,
        priority : 1.00,
        changeFrequency : 'daily'
    },
    {
        name     : AboutRoute,
        location : process.env.PRODUCTION_APP_URL + AboutRoute,
        priority : 1.00,
        changeFrequency : 'daily'
    },
];

