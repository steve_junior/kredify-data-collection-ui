import React from 'react';
import {
    AboutRoute,
    GetStartedClientRoute,
    GetStartedMerchantRoute,
    HomeRoute,
    WelcomeAboardRoute
} from "./custom-routes";

export function isDefined(value){
    return typeof value !== "undefined" && value !== null;
}

export function isStrictlyDefined(value) {
    return typeof value !== "undefined" && value !== null;
}

/**
 * @return {object}
 */
export function Company() {
    const domain = "kredify.net";

    return  {
      name : "Kredify",
      enquiry :{
          email : "enquiry@" + domain,
      },
      support : {
          email : "support@" + domain,
          contact : "233 541 822 392"
      },
      domain  : domain,
      address : <address><p>43 Park Avenue,</p><p>Teshie Nungua</p><p>Accra</p></address>
    };
}


export function out(value, type = 'log') {

    if(process.env.DEBUG !== 'true'){
        return;
    }

    switch (type){
        case 'log':
            console.log(value);
            break;
        default:
            console.log(value);
    }
}

export function routeTo(url = HomeRoute) {
    return window.location.assign(url);
}

export function getUrlParameter(url, name) {
    name        = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    let regex   = new RegExp('[\\?&]' + name + '=([^&#]*)');
    let results = regex.exec(url);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

    export function getPageTitle(routeName = HomeRoute) {
   let pageTitle = '';

    switch (routeName){
        case HomeRoute:
            pageTitle  = 'Home';
            break;
        case GetStartedClientRoute:
            pageTitle  = 'Get Started';
            break;
        case GetStartedMerchantRoute:
            pageTitle = 'Onboard Merchant';
            break;
        case AboutRoute:
            pageTitle  = 'About Us';
            break;
        case WelcomeAboardRoute:
            pageTitle  = 'Welcome';
            break;
        default:
            pageTitle = 'Home';
    }

    return pageTitle;
}