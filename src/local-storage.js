export class WebStore {
    constructor(){
        if (typeof(Storage) !== "undefined") {
            // Code for localStorage/sessionStorage.
        } else {
            // Sorry! No Web Storage support..
        }
    }

    static retrieve(key) {
        return JSON.parse(sessionStorage.getItem(key));
    }

    static store(key, value){
        if(value === null) return;
        sessionStorage.setItem(key, JSON.stringify(value));
    }
}