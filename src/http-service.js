import fetch from 'isomorphic-unfetch';
import {out} from "./helpers";

const api_url = process.env.API_URL;

const coming_soon_url  = api_url + "coming-soon/";

const header = {
    'Accept' : 'application/json'
};

function request_option(method = 'POST', body = null) {
    return { body, method, header : header};
}

async function http_call(url, request_options){
    out(request_options);
    out(url);
    try {
        const request  = await fetch(url, request_options);
        return await request.json();
    }catch (e) {
        console.log(e);
        return null;
    }
}

export async function get_config_data() {
    const url = coming_soon_url + "get-app-data";
    const option = request_option();
    return await http_call(url, option);
}

export async function onboard_client(body){
    const parameters = new FormData();
    for(let [key, value] of Object.entries(body)){
        parameters.append(key, value)
    }
    const url = coming_soon_url + "onboard-client";
    const option = request_option('POST', parameters);
    return await http_call(url, option);
}

export async function onboard_merchant(body){
    const parameters = new FormData();
    for(let [key, value] of Object.entries(body)){
        parameters.append(key, value)
    }
    const url = coming_soon_url + "onboard-merchant";
    const option = request_option('POST', parameters);
    return await http_call(url, option);
}

export async function contact_us(body){
    const parameters = new FormData();
    for(let [key, value] of Object.entries(body)){
        parameters.append(key, value)
    }
    const url = coming_soon_url + "contact-us";
    const option = request_option('POST', parameters);
    return await http_call(url, option);
}

export async function subscribe_to_news_letter(body){
    const parameters = new FormData();
    for(let [key, value] of Object.entries(body)){
        parameters.append(key, value)
    }
    const url = coming_soon_url + "subscribe-news-letter";
    const option = request_option('POST', parameters);
    return await http_call(url, option);
}
